# Wikimedia Germany Technical Wishes docker-dev development environment

## Overview

The [Wikimedia Deutschland Technical Wishes
team](https://meta.wikimedia.org/wiki/WMDE_Technical_Wishes) has created this
codebase in order to set up and share an internal development environment,
specific to our features.  If you're looking for a general environment for
MediaWiki, please see the related [upstream project](https://www.mediawiki.org/wiki/MediaWiki-Docker).

## Installation

Clone the docker-dev repository into a local working directory of your choice.

Check if you have an up to date MediaWiki development repository running and if you want to reuse it.  If not, clone mediawiki-core into the docker-dev directory.  Also clone the Vector and MinervaNeue skins.

    git clone --depth=1 https://gerrit.wikimedia.org/r/mediawiki/core mediawiki
    git clone https://gerrit.wikimedia.org/r/mediawiki/skins/Vector mediawiki/skins/
    git clone https://gerrit.wikimedia.org/r/mediawiki/skins/MinervaNeue mediawiki/skins/

The `--depth=1` flag is optional, it saves some network and disk space at the cost of not having the full change history available.

If you already have an existing MediaWiki development repository, you can symlink it from the docker-dev directory:

    ln -s ~/mediawiki-core ./mediawiki

If you have an existing `./mediawiki/LocalSettings.php` rename it to `./mediawiki/LocalSettings.native.php`. Configuration settings in `LocalSettings.native.php` will not be used in the docker environment but will be used in the "bare metal" environment.  A LocalSettings.php shim is created, which switches between the different contexts based on an environment variable.

## Running

To start the containers with the bare defaults,

    ./bin/use

Connect to the default site http://dev.wiki.local.wmftest.net:8080/ , log in with `admin` / `dockerpass`.

To have MediaWiki listen on an alternate port set the environment variable like,

    MW_DOCKER_PORT=8081 ./bin/use

To run the `mwscript` wrapper for maintenance scripts, call like this:

    ./modules/mediawiki/bin/mwscript runJobs.php --wiki dev

## Configuration and `LocalSettings.d`

The main "mediawiki" module installs a special [`LocalSettings.php`](./modules/mediawiki/LocalSettings.shim.php) shim which can delegate to your existing local configuration when running the checked-out code on bare metal.  It renames your existing file to `LocalSettings.native.php` for clarity.

When running in the docker environment, the configuration shim will read all files from `LocalSettings.d/*.php`, which contains symlinks to each `modules/<enabled_module>/LocalSettings.d` directory for each enabled module.

If you want the config symlinks in LocalSettings.d to also work on the host machine for convenience, you must manually create the redirect.

    ln -sf $YOUR_DOCKER_DEV_ROOT /srv/docker-dev

We suggest that you make any configuration changes during development in the specific file for that module, so that your change is ready to be committed back to the repo.  General configuration can be set in the ["default" module settings](./modules/defaults/LocalSettings.d/00-defaults.php)

## Debugging

Many debugging option are enabled by default.  Logs can be found in the `./logs` directory.  Some extra debugging options are available in `00-defaults.php` but were left commented out because the cause extra disruption, eg. to the page layout.

## Modules

Configuration, extensions, and other modifications to MediaWiki are each provisioned by a "module".  These are directories under `./modules`, and any number of them can be enabled like:

    ./bin/use CentralAuth VisualEditor

This will run any `modules/$MODULE/bin/up` script for the module if available, which clones extensions, copies `LocalSettings.d` entries, imports `xml-dumps`, may set up dockerized services, runs maintenance scripts, and so on.

More documentation about each module can be found in its local README, linked from the title.

### Local default `.modules.conf`

To save a list of modules as your default configuration, save to a config file `.modules.conf`, and these modules will be loaded any time you call "use" without modules.

    echo "Cite VisualEditor" > .modules.conf
	./bin/use

The set of enabled modules can still be overridden by passing modules to "use" explicitly: `./bin/use AdvancedSearch`

### AbuseFilter

Enables [AbuseFilter](https://www.mediawiki.org/wiki/Extension:AbuseFilter) and installs a rule which will recoverably warn-fail when an edit contains "warnme".

### AdvancedSearch

Provides the AdvancedSearch extension also adding the `elastic` module as dependency.

### CentralAuth

Starts a separate virtual host "centralauth.wiki.local.wmftest.net" and configures the CentralAuth extension to provide unified login and authentication.

### Cite

Provision the Cite module and enable the "book referencing" feature-in-development.

### Citoid

Provision the Citoid extension and the Node.js services for the auto generation of references. Set's up needed .json
configuration and two example templates `Cite news`and `Cite web` with minimal configuration to be used for testing with
real websites.

Automatically loads `Cite` and `VisualEditor` modules that are needed to even interact with the tool and `TemplateData`
that allows playing with the templates used for the automatic creation. 

### CodeMirror

Provides the CodeMirror extension and enables it by default. Comes with the WikiEditor to make use of CodeMirror.

### CommunityConfiguration

Visit these URLs to play with the feature:

* http://dev.wiki.local.wmftest.net:8080/wiki/Special:CommunityConfiguration
* http://dev.wiki.local.wmftest.net:8080/wiki/Special:CommunityConfiguration/CommunityConfigurationExample
* http://dev.wiki.local.wmftest.net:8080/wiki/Special:CommunityConfigurationExample

### FileImporter

Provides FileImporter and FileExporter extensions to import files from one wiki to another. The default `dev` wiki is set up with the FileExporter extension so a file can be exported. The `commons` wiki is set up with the FileImporter extension to import files.

It's currently set up to allow imports from any wiki that's listed on the [local configuration](http://commons.wiki.local.wmftest.net:8080/wiki/Extension:FileImporter/Data). You can test it with [this file](http://dev.wiki.local.wmftest.net:8080/wiki/File:CuteGoat.jpg), or upload your own.  Delete the file on commons in order to import again.

Automatically loads `AbuseFilter` module and sets up a rule that will recoverably warn-fail FileImporter when the page title includes "warnme".

While testing the FileImporter the image might get moved or removed from the storage. There's a script that reuploads
the original file to the `dev` wiki. Run it by executing

    ./modules/FileImporter/bin/reset-images

from the docker dev directory.

#### Limitations

- SUL isn't working yet, so FileImporter won't make edits on the source wiki.

### Gadgets

Installs the Gadgets extension and imports a sample gadget.

### GeoData

Installs GeoData extension, to support the `geosearch` API used by the "Nearby" feature. Automatically loads `elastic` 
module to enable the search. Comes with a basic example set for articles that show how `relevance` can get a higher
ranking in search results than `distance`.

### GeoDataStubs

Random set of 1000 articles with geolocations to play with geosearch results.

### Kartographer

This module configures the Kartographer extension, but doesn't provide a Kartotherian server. Automatically loads
`GeoData` module to enable the "Nearby" feature. Might be usefull to load this together with the `GeoDataStubs` module
when there should be a bit more test data for the latter.

To start a containerized development server, see the separate [kartodock](https://github.com/wikimedia/kartodock/) project and start that locally, to serve port 6533.

### mariadb

To open an interactive mysql session,

    ./modules/mariadb/bin/db-docker-compose exec database mysql

### mediawiki

#### Running maintenance scripts

To run a maintenance script,

    ./modules/mediawiki/bin/mwscript version.php --wiki dev

#### Generating PHP docs

A wrapper is provided for Doxygen,

    ./modules/mediawiki/bin/doxygen --file extensions/Cite

Generated output can be found in `mediawiki/docs/html` .

### MobileFrontend

Provides the mobile skin.

### php

To use a PHP release other than the default,

    DOCKER_PHP_VERSION=8.1 ./bin/use

### RevisionSlider

Provision the RevisionSlider extension.

### survey-dashboard

This sets up an Elixir Phoenix application running our https://gitlab.com/wmde/technical-wishes/survey-tools/survey-dashboard .  The source is cloned to `modules/survey-dashboard/src`, and the site can be reached at http://survey-dashboard.local.wmftest.net:4001/

For logs, run: `docker compose -f modules/survey-dashboard/docker/docker-compose.yml logs`

Limitations:
- No live reload when source files are changed.
- File ownership can be glitchy if sharing files with a bare-metal instance.

### TwoColConflict

Provision the TwoColConflict extension and enables it by default.

### VisualEditor

Sets up VE and enable feature flags for currently developed WMDE Technical Wishes projects.

This extension has submodules, that might need updating from time to time. 
    git submodule update --init

#### ParserMigration

This extension is enabled by default. It allows both anonymous and admin users to compare article rendering between the default Legacy Parser and the new Parsoid parser.

The options "Switch to legacy parser" and "Switch to new parser (Parsoid)" are accessible via the frontend toolbar.

### wikidiff2

Builds and enables the php extension for wikidiff2, which is needed for inline diff support.

### collab

Sets up real-time collaboration using VisualEditor.

### elastic

Elasticsearch server and MediaWiki integration.

TODO: document data persistence

### [eventlogging](./modules/eventlogging/README.md)

Event logging server and MediaWiki integration.

TODO: Provision local event schema repositories for schema development.

### [surveys](./modules/surveys/README.md)

QuickSurveys.

## Testing

### PHPUnit tests

Run extension unit tests like this:

    ./modules/mediawiki/bin/phpunit extensions/TemplateData/tests/phpunit

Make sure, that the extension is loaded e.g. with `./bin/use TemplateData`.

### QUnit tests

The docker-dev environment includes a cheap clone of the [fresh](https://github.com/wikimedia/fresh/) tool, providing the same execution environment as Wikimedia CI uses for nodejs 20 browser testing.

Install and self-test with,

    ./modules/mediawiki/bin/fresh-node node --version

Run QUnit on an extension,

    ./modules/mediawiki/bin/fresh-node npx grunt qunit --qunit-component=Cite

### QUnit tests with external fresh-node

If you would like to use the fresh-node tool as an alternative to docker-dev, it's compatible as long as you set your environment correctly as below.

See also [Manual:JavaScript_unit_testing](https://www.mediawiki.org/wiki/Manual:JavaScript_unit_testing)

Install `fresh-node` from

    https://gerrit.wikimedia.org/g/fresh/

Add an `.env` config file to the core directory with the following content

    MW_SERVER=http://dev.wiki.local.wmftest.net:8080
    MW_SCRIPT_PATH=/w/

Start `fresh-node` from inside the core directory with

    fresh-node -net -env

Run QUnit tests with

    npm run qunit

Run QUnit tests for just one component with

    node_modules/grunt/bin/grunt qunit --qunit-component=RevisionSlider

**Note:** For the `FileImporter` that's only installed on the `commons` wiki you need to adjust the .env to use

    MW_SERVER=http://commons.wiki.local.wmftest.net:8080

### Selenium tests with fresh-node

See also [Selenium/Getting_Started/Run_tests_using_Fresh](https://www.mediawiki.org/wiki/Selenium/Getting_Started/Run_tests_using_Fresh)

`fresh-node` does not only offer a good wrapper for managing and running node dependencies, but also includes all tools needed for running selenium tests.

Install `fresh-node` from 

    https://gerrit.wikimedia.org/g/fresh/

Add an `.env` config file to the extension's directory where you want to run the test from with the following content

    MW_SERVER=http://dev.wiki.local.wmftest.net:8080
    MW_SCRIPT_PATH=/w/
    MEDIAWIKI_USER=admin
    MEDIAWIKI_PASSWORD=dockerpass

Start `fresh-node` from inside the extension's directory with

    fresh-node14 -net -env

Run browser tests with

    npm run selenium-test

**Note:** For the `FileImporter` that's only installed on the `commons` wiki you need to adjust the .env to use

    MW_SERVER=http://commons.wiki.local.wmftest.net:8080

### Cypress end-to-end tests

**Prerequisites**

Ensure you are using Node.js version 16 or higher

Add an `.env` config file to the mediawiki directory with the following content

    MW_SERVER=http://dev.wiki.local.wmftest.net:8080
    MW_SCRIPT_PATH=/w/
    MEDIAWIKI_USER=admin
    MEDIAWIKI_PASSWORD=dockerpass

**Running Cypress Tests**

Navigate to the extension’s directory:

Run in headed mode

    CYPRESS_CACHE_FOLDER=./tests/cypress/.cache npx cypress open --config-file tests/cypress/cypress.config.js

Run in headless mode

    CYPRESS_CACHE_FOLDER=./tests/cypress/.cache npx cypress run --config-file tests/cypress/cypress.config.js


Or use scripts from package.json file

Run in headed mode 

    npm run cypress:open

Run in headless mode 

    npm run selenium-test

Run only a specific test cases by adding `.only()` to the `it` case

    it.only('should perform specific action', () => {
        // Test code
    });

To repeat a test set run for e.g. 50 times wrapp it in a 

    Cypress._.times( 50, () => { }

block.

**Notes**

Test Directory:
Cypress test files should be organized within the tests/cypress directory of your extension.

Configuration File:
Customize Cypress configurations and other settings in tests/cypress/cypress.config.js

### Parser tests

To run all parser tests,

    ./modules/mediawiki/bin/mwscript tests/parser/parserTests.php --wiki=dev

To run tests from a specific file,

    ./modules/mediawiki/bin/mwscript tests/parser/parserTests.php --wiki=dev --file=/srv/docker-dev/mediawiki/extensions/Cite/tests/parser/citeParserTests.txt

And to limit to a single case, use `--filter` or `--regex`

    ./modules/mediawiki/bin/mwscript tests/parser/parserTests.php --wiki=dev --file=/srv/docker-dev/mediawiki/extensions/Cite/tests/parser/citeParserTests.txt --filter T202593

To run Parsoid parser tests, add the `--parsoid` flag for all flavors, or
specify test variant using `--wt2html` `--wt2wt` `--html2wt` or `--html2html`.

    ./modules/mediawiki/bin/mwscript tests/parser/parserTests.php --wiki=dev --file=/srv/docker-dev/mediawiki/extensions/Cite/tests/parser/citeParserTests.txt --parsoid

### Phan tests

To run Phan locally,

    ./modules/mediawiki/bin/phan extensions/Cite

But, please note that the output may differ from CI where Phan is currently running under PHP 7.4

## Container debugging

To run commands inside the container itself, you can log in with,

    ./modules/php/bin/php-docker-compose exec php bash

To follow the MediaWiki logs,

    docker compose -f docker/docker-compose.yml logs -f

For other services, first you need to know which docker-compose file is responsible for the container you're looking at.  Then, run a command like:

    docker compose -f modules/maps/docker/docker-compose.yml logs -f

## Troubleshooting

This section lists some issues you might run into, and their solutions.

### Cache directory permissions

> /app/cache/composer/vcs does not exist and could not be created.

The `mediawiki/cache` directory must be writeable by the web user.  Best to just make it writeable by everyone:

  chmod a+w mediawiki/cache

### Composer incompatibilities

> Your requirements could not be resolved to an installable set of packages.

Update MediaWiki core, extensions and vendor to the latest versions.  Even the extensions which are not enabled can cause trouble, thanks to the composer-merge-plugin!  Use this cheat code to update everything:

  for d in ./mediawiki/{extensions,skins}/*/ ; do (cd "$d" && git checkout master; git pull); done

If composer itself needs an update, use this command:

  docker pull composer:2

### Missing interface messages

If you see placeholder messages in the interface, then the localization cache may need to be refreshed.  Try this command to refresh the English messages,

  ./modules/mediawiki/bin/mwscript rebuildLocalisationCache.php --wiki=dev --lang en --force
