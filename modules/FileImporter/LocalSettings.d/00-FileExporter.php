<?php

# https://www.mediawiki.org/wiki/Extension:FileExporter

if ( $wgDBname === 'dev' ) {
	wfLoadExtension( 'FileExporter' );

	$wgFileExporterTarget = 'http://commons.wiki.local.wmftest.net:' . getenv( 'MW_DOCKER_PORT' ) . '/wiki/Special:ImportFile';
}
