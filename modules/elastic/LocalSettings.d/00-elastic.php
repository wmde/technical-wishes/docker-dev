<?php

wfLoadExtension( 'Elastica' );

require_once "$IP/extensions/CirrusSearch/tests/jenkins/FullyFeaturedConfig.php";
wfLoadExtension( 'CirrusSearch' );
$wgCirrusSearchServers = [ 'elasticsearch' ];
$wgCirrusSearchLogElasticRequests = true;
