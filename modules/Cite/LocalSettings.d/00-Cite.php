<?php

wfLoadExtension( 'cldr' );
wfLoadExtension( 'Cite' );

$wgCiteSubReferencing = true;
$wgCiteUseLegacyBacklinkLabels = false;
$wgCiteDefaultBacklinkAlphabet = 'z y x';
$wgCiteBacklinkCommunityConfiguration = true;
