This module provides a QuickSurveys configuration.

Results can only be captured by also installing the "eventlogging" sibling module.

To configure surveys, edit the LocalSettings.d/00-QuickSurveys.php file.

When nothing is working, debug the ResourceLoader stuff by visiting http://dev.wiki.local.wmftest.net:8080/w/load.php?modules=ext.quicksurveys.lib&debug=true&only=scripts

Note that surveys don't appear on the wiki's Main Page, or on pages that don't exist.

To force a survey to appear, for example even after you have tokens in local storage or if the survey coverage is low, use the `quicksurvey` URL parameter: http://dev.wiki.local.wmftest.net:8080/w/index.php?title=Blank&quicksurvey=external%20example%20survey
