<?php

wfLoadExtension( 'ParserMigration' );

# Uncomment to enable parsoid on all article pages by default.
# $wgParserMigrationEnableParsoidArticlePages = true;
$wgParserMigrationEnableQueryString = true;
$wgDefaultUserOptions[ 'parsermigration' ] = true;
