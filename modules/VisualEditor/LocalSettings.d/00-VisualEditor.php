<?php

wfLoadExtension( 'Parsoid', "{$IP}/vendor/wikimedia/parsoid/extension.json" );
wfLoadExtension( 'VisualEditor' );

$wgVisualEditorNewTemplateSearch = true;
$wgGroupPermissions['user']['writeapi'] = true;
$wgVisualEditorEnableWikitextBetaFeature = false;
$wgDefaultUserOptions['visualeditor-enable'] = 1;
$wgVisualEditorEnableWikitext = true;

# Uncomment to log at debug level.
# $wgDebugLogGroups[ 'VisualEditor' ] = '/srv/log/VisualEditor.log';
