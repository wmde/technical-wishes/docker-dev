<?php

# Protect against web entry

if ( !defined( 'MEDIAWIKI' ) ) {
    exit;
}

# Site

$wiki = false;
if ( isset( $_SERVER['SERVER_NAME'] ) ) {
	$serverParts = explode( '.', $_SERVER['SERVER_NAME'] );
	$wiki = array_shift( $serverParts );
}
if ( !$wiki && defined( 'MW_DB' ) ) {
	$wiki = MW_DB;
}
if ( !$wiki ) {
	die( 'Cannot detect the wiki.  Please set MW_DB in the environment.' );
}
$wgServer = 'http://' . ( getenv( 'SERVER_NAME' ) ?: 'dev.wiki.local.wmftest.net' ) .
	':' . getenv( 'MW_DOCKER_PORT' );
$wgSitename = "docker-$wiki";
$wgMetaNamespace = "Docker-$wiki";

$wgUsePathInfo = false;
$wgArticlePath = "/wiki/$1";
$wgScriptPath = "/w";
$wgResourceBasePath = $wgScriptPath;

$wgLogos = [ '1x' => "$wgResourceBasePath/resources/assets/wiki.png" ];

# Database

$wgDBtype = "mysql";
$wgDBserver = "database";
$wgDBname = $wiki;
$wgDBuser = "root";
$wgDBpassword = "";

$wgDBprefix = "";
$wgDBTableOptions = "ENGINE=InnoDB, DEFAULT CHARSET=binary";

# Email

$wgEnableEmail = false;

# Caching

$wgMainCacheType = CACHE_NONE;
$wgParserCacheType = CACHE_NONE;
$wgCachePages = false;
$wgMemCachedServers = [];

# Files

$wgMaxUploadSize = 1024 * 1024 * 512; // 512MB
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";

$wgUseInstantCommons = false;
$wgPingback = false;

# Locale

$wgShellLocale = "C.UTF-8";
$wgLanguageCode = "en";

# Keys

$wgSecretKey = "76d3da80886ae7d10f3448b7e194217ef92b2c5e3ad5e209dd87d5423512932b";
$wgAuthenticationTokenVersion = "1";
$wgUpgradeKey = "89953b5f135f9b0e";

# Diff

$wgDiff3 = "/usr/bin/diff3";

# Skins

$wgDefaultSkin = 'vector-2022';
wfLoadSkin( 'Vector' );

$wgVectorNightMode = [
	'logged_out' => true,
	'logged_in' => true,
	// Important to disable this to disable the beta feature
	'beta' => false,
];

// Automatic dark mode.
$wgDefaultUserOptions['vector-theme'] = 'os';

# Debugging options

# Uncomment these options for a lot of additional logging.
# $wgDebugLogFile = '/srv/log/mediawiki-debug.log';
# $wgDebugToolbar = true;

// Note that you can enable this per-request using the `?debug=true` URL parameter.
# $wgResourceLoaderDebug = true;

require "$IP/includes/DevelopmentSettings.php";

wfLoadExtensions( [
	'ParserFunctions',
] );
