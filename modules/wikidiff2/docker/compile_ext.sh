#!/bin/bash

if php -i | grep -q -E 'wikidiff2.*enabled'; then
	echo "wikidiff2 already installed"
else
	apt update && apt install -y libthai-dev && apt purge

	cd src
	phpize
	./configure
	make && make install
	docker-php-ext-enable wikidiff2

	apache2ctl graceful
fi
