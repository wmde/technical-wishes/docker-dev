#!/bin/bash

# Fail closed and verbose.
set -euxo pipefail

# Pull down dependencies if needed.
if [ ! -e /srv/collab/node_modules ]; then
    pushd /srv/collab/
    npm install
    popd
fi

if [ ! -e /srv/collab/dist ]; then
    pushd /srv/collab/
    grunt build
    popd
fi

if [ ! -e /srv/collab/rebaser/node_modules ]; then
    pushd /srv/collab/rebaser/
    npm install
    popd
fi

# Watch source and restart the server when changed.
nodemon -e js,json,yaml --watch /srv/collab/rebaser --signal SIGHUP src/server.js
