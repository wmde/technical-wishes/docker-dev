This module provides the EventLogging service to receive events.

To follow the event log, run:

    tail -f mediawiki/extensions/EventLogging/devserver/events.json

or to see all server messages such as validation errors,

    docker compose --file modules/eventlogging/docker/docker-compose.yml logs -f

To test logging from the browser console,

    mw.eventLog.submit('test.event', {'$schema': '/test/event/1.0.0', 'test': window.location.href})
