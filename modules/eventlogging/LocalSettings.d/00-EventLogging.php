<?php

wfLoadExtension( 'EventLogging' );

// This URL must be reachable both from the php container and from the browser.
$wgEventLoggingServiceUri = 'http://eventlogging.local.wmftest.net:8192/v1/events/';

// By default EventLogging waits 30 seconds before sending
// batches of queued events.  That's annoying in a dev env.
$wgEventLoggingQueueLingerSeconds = 1;

// By settings $wgEventLoggingStreamNames to false, we instruct EventLogging
// to not use any EventStreamConfig. Instead, all streams will be seen as
// if they are configured and registered. See the EventStreamConfig
// [README](https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/extensions/EventStreamConfig/+/master/README.md//mediawiki-config)
// for instructions on how to set up stream config.
$wgEventLoggingStreamNames = false;
