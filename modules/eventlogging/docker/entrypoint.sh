#!/bin/bash

# Fail closed and verbose.
set -euxo pipefail

# Pull down dependencies if needed.
if [ ! -e /srv/devserver/node_modules ]; then
    cd /srv/devserver
    npm install
fi

npm run eventgate-devserver
