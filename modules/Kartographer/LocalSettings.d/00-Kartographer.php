<?php

wfLoadExtension( 'JsonConfig' );
wfLoadExtension( 'Kartographer' );
wfLoadExtension( 'PageImages' );

$wgKartographerStaticMapframe = true;
$wgKartographerEnableMapFrame = true;
$wgKartographerUseMarkerStyle = true;
$wgKartographerMapServer = "http://maps.local.wmftest.net:6533";
$wgKartographerMediaWikiInternalUrl = 'http://dev.wiki.local.wmftest.net:' . getenv( 'MW_DOCKER_PORT' );
$wgKartographerVersionedLiveMaps = true;
$wgKartographerVersionedMapdata = true;
$wgKartographerVersionedStaticMaps = true;
$wgKartographerNearby = true;
$wgKartographerNearbyClustering = true;
$wgKartographerNearbyOnMobile = true;
$wgKartographerWikivoyageMode = true;
$wgKartographerStyles = [ 'osm-pbf', 'osm', 'osm-intl' ];

// Copied from https://www.mediawiki.org/wiki/Extension:JsonConfig#Supporting_Wikimedia_templates
$wgJsonConfigEnableLuaSupport = true;
$wgJsonConfigModels['Map.JsonConfig'] = 'JsonConfig\JCMapDataContent';
$wgJsonConfigs['Map.JsonConfig'] = [
        'namespace' => 486,
        'nsName' => 'Data',
        // page name must end in ".map", and contain at least one symbol
        'pattern' => '/.\.map$/',
        'license' => 'CC0-1.0',
        'isLocal' => false,
];
$wgJsonConfigInterwikiPrefix = "commons";
$wgJsonConfigs['Map.JsonConfig']['remote'] = [
        'url' => 'https://commons.wikimedia.org/w/api.php'
];

$wgDebugLogGroups[ 'Kartographer' ] = '/srv/log/Kartographer.log';
